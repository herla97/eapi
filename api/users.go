package api

import (
	"log"
	"net/http"

	"github.com/jinzhu/gorm"
	"github.com/labstack/echo/v4"
	"gitlab.com/herla97/eapi/models"
)

// UsersGet Find all users
func UsersGet(c echo.Context) error {
	db, _ := c.Get("db").(*gorm.DB)

	users := &models.Users{}

	if err := db.Find(users).Error; err != nil {

	}

	return c.JSON(http.StatusOK, users)
}

// UsersCreate Find all users
func UsersCreate(c echo.Context) error {
	// User Model
	user := &models.User{}

	if err := c.Bind(user); err != nil {
		return err
	}

	db, _ := c.Get("db").(*gorm.DB)

	if err := user.HashPassword(); err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, "HashPassword error")
	}

	if err := db.Create(user).Error; err != nil {
		log.Println(err)
		return echo.NewHTTPError(http.StatusInternalServerError, "User can not Create")
	}

	return c.JSON(http.StatusCreated, user)
}
