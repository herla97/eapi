package routers

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/herla97/eapi/api"
)

func Init() *echo.Echo {
	e := echo.New()

	e.GET("/", api.Home)
	e.GET("/users", api.UsersGet)
	e.POST("/users", api.UsersCreate)

	return e
}
