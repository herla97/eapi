package models

import (
	"golang.org/x/crypto/bcrypt"
)

// User Model
type User struct {
	Base
	Name     string `json:"name" gorm:"type:varchar(100)"`
	Username string `json:"username" gorm:"type:varchar(100)" `
	Password string `json:"password" gorm:"type:varchar(255)" `
}

// Users type
type Users []User

//HashPassword substitutes User.Password with its bcrypt hash
func (user *User) HashPassword() error {
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return err
	}
	user.Password = string(hash)
	return nil
}
