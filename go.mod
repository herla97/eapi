module gitlab.com/herla97/eapi

go 1.14

require (
	github.com/davecgh/go-spew v1.1.0
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/jinzhu/gorm v1.9.12
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.1.16
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d
)
