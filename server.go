package main

import (
	"log"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/herla97/eapi/db"
	"gitlab.com/herla97/eapi/middlewares"
	"gitlab.com/herla97/eapi/routers"
)

func init() {
	if err := godotenv.Load(); err != nil {
		log.Fatalf("Error getting env, not comming through %v", err)
	}
}

func main() {
	db := db.Connect()
	e := routers.Init()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middlewares.ContextDB(db))

	e.Logger.Fatal(e.Start(":1323"))
}
